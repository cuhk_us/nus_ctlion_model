
% clear the workspace and close the figures
close all; clc;
clear variables;
clear global;
clear QrotorSimPX4; 


%% set the parameters
Ts = 0.02;  % 50 Hz
Td = 5; 
t  = 0:Ts:Td;
n  = length(t); 

g  = 9.781; 

%% set the reference
x_r   = zeros(length(t),1);
y_r   = zeros(length(t),1);
z_r   = zeros(length(t),1);
ug_r  = zeros(length(t),1);
vg_r  = zeros(length(t),1);
wg_r  = zeros(length(t),1);
agx_r = zeros(length(t),1);
agy_r = zeros(length(t),1);
agz_r = zeros(length(t),1);
psi_r = zeros(length(t),1);
r_r   = zeros(length(t),1);

selRef = 6;

switch selRef
    case 1,
        x_r(:) = 15;
        psi_r( 1/Ts*20: 1/Ts*50 ) = 0;
    
    case 2, % circling
       a = 2; % rad/s
       r = 2;    %  m
       
       for i = 1:length(t)    
           x_r(i)  = r * cos( a * t(i) ); 
           y_r(i)  = r * sin( a * t(i) ); 
           z_r(i)  = 1; 

           ug_r(i) = - a * r * sin( a * t(i) ); 
           vg_r(i) =   a * r * cos( a * t(i) ); 
           wg_r(i) =   0; 
           
           agx_r(i) = - a * a * r * cos( a * t(i) ); 
           agy_r(i) = - a * a * r * sin( a * t(i) );
           agz_r(i) =  0;
           
           %psi_r(i) = pi / 2 + a * t(i);
           %psi_r_raw = 0.1 * t(i);
           %psi_r(i)  = psi_r_raw - floor( (psi_r_raw+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]
           
           psi_r(i) = 0.1 * t(i);
           r_r(i)   = 0.1; 
       end
          
    case 3, % forward flight
        for i = 1:length(t)     
            x_r(i)  = 8.5 * t(i); 
            y_r(i)  = 0; 
            z_r(i)  = 1; 
            
            psi_r(i) = 0;
        end
        
    case 4, % path planning simulation from Dr Liao Fang
        load UAVsimdata0.mat; 
        n = length(UAV_ref); 
        
        x_r(1:n,1)   = UAV_ref(1,:)';
        y_r(1:n,1)   = UAV_ref(2,:)';
        z_r(1:n,1)   = UAV_ref(3,:)';
        ug_r(1:n,1)  = UAV_ref(4,:)';
        vg_r(1:n,1)  = UAV_ref(5,:)';
        wg_r(1:n,1)  = UAV_ref(6,:)';
        agx_r(1:n,1) = UAV_ref(7,:)';
        agy_r(1:n,1) = UAV_ref(8,:)';
        agz_r(1:n,1) = UAV_ref(9,:)';
        psi_r(1:n,1) = UAV_ref(10,:)';
        r_r(1:n,1)   = UAV_ref(11,:)';
        
        
   case 5, % path planning simulation from Dr Liao Fang
        UAV_ref = load('ref.txt'); 
        n = length(UAV_ref); 
        UAV_ref = UAV_ref'; 
        
        x_r(1:n,1)   = UAV_ref(1,:)';
        y_r(1:n,1)   = UAV_ref(2,:)';
        z_r(1:n,1)   = UAV_ref(3,:)';
        ug_r(1:n,1)  = UAV_ref(4,:)';
        vg_r(1:n,1)  = UAV_ref(5,:)';
        wg_r(1:n,1)  = UAV_ref(6,:)';
        agx_r(1:n,1) = UAV_ref(7,:)';
        agy_r(1:n,1) = UAV_ref(8,:)';
        agz_r(1:n,1) = UAV_ref(9,:)';
        psi_r(1:n,1) = UAV_ref(10,:)';
        r_r(1:n,1)   = UAV_ref(11,:)';
        
    case 6, % path planning simulation from D-Drone, #1
       
        fileID = fopen('..\nus_ctlion_tested_traj\2018-09-25\traj2.txt');
        fileData = fscanf(fileID, ...
                          [ '- [' '%f' ',' '%f' ',' '%f' ',' ...    
                                 '%f' ',' '%f' ',' '%f' ',' ...
                                 '%f' ',' '%f' ',' '%f' ',' ...
                                 '%f' ']\n'], [10, inf]);
        fclose(fileID); 
        
        UAV_ref = fileData(:,2:end); % reference is in NWU frame;
                            % we also remove the first column, which is not
                            % used
        
        n = length(UAV_ref); 
        
        x_r(1:n,1)   =  UAV_ref(1,:)';
        y_r(1:n,1)   = -UAV_ref(2,:)';
        z_r(1:n,1)   = -UAV_ref(3,:)';
        ug_r(1:n,1)  =  UAV_ref(4,:)';
        vg_r(1:n,1)  = -UAV_ref(5,:)';
        wg_r(1:n,1)  = -UAV_ref(6,:)';
        agx_r(1:n,1) =  UAV_ref(7,:)';
        agy_r(1:n,1) = -UAV_ref(8,:)';
        agz_r(1:n,1) = -UAV_ref(9,:)';
        psi_r(1:n,1) = -UAV_ref(10,:)';
        r_r(1:n,1)   =  zeros(n,1);
       
        t  = 0:Ts:(n-1)*Ts;
        
        %-- load the real flight test data
        %pathname = '..\nus_ctlion_px4_log\log_drone1\sess097\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #1, 3 drones teaming, trial 2
        pathname = '..\nus_ctlion_px4_log\log_drone2\sess193\'; % ocotrotor test, 25 Sep, fast ellipse flight test, #2, 3 drones teaming, trial 2

        dataname        = 'result01'; 
        fullfilename    = [pathname, dataname, '.csv']; 
        [num, txt, raw] = xlsread(fullfilename);

        data = num(3:end, :);
        
        px4.t = data(:,340)*0.000001; % time
        px4.phi = data(:,1); % roll angle
        px4.tht = data(:,2); % pitch angle
        px4.psi = data(:,3); % yaw angle

        px4.p   = data(:,4); % roll anglular rate
        px4.q   = data(:,5); % pitch anglular rate
        px4.r   = data(:,6); % yaw anglular rate
        
        px4.abx = data(:,14); % imu x acceleration in body axis
        px4.aby = data(:,15); % imu y acceleration in body axis
        px4.abz = data(:,16); % imu z acceleartion in body axis
        
        for ii=1:length(px4.abx)
            phi = px4.phi(ii);
            tht = px4.tht(ii);
            psi = px4.psi(ii);

            R = R_Body2NED(phi, tht, psi); 
            
            % convert to NED frame
            acg = R * [ px4.abx(ii); px4.aby(ii); px4.abz(ii) ]; 
            px4.agx(ii) = acg(1);
            px4.agy(ii) = acg(2);
            px4.agz(ii) = acg(3) + g;
        end 
   
        px4.x = data(:,46); % N position
        px4.y = data(:,47); % E position
        px4.z = data(:,48); % D position

        px4.ug = data(:,51); % N velocity 
        px4.vg = data(:,52); % E velocity 
        px4.wg = data(:,53); % D velocity 
        
end

    

%% set the initial values for the states
xn    = zeros(15, 1); 

xn(1) =  2; 
xn(2) = -10; 
xn(3) = -3.5;

xn(9) = 0;

xc      = xn;
a_b     = [0; 0; 0];
xn_all  = zeros(n, 15); 
a_b_all = zeros(n, 3); 
%uin_all = zeros(n, 4); 
uin_all = [];

%Itg     = [xn(1:3); x_r(1); y_r(1); z_r(1); xn(12); psi_r(1)];
Itg     = zeros(8,1);

uin_r   = [0; 0; 0; 0];


%% start to run the quadrotor dynamics model with conveter

disp('start the simulation...');
for i = 1:length(t)    
    
    display(i); 
    
    if i == 1146
        temp = 1; 
    end
    
    refer = [x_r(i); y_r(i); z_r(i); ug_r(i); vg_r(i); wg_r(i); agx_r(i); agy_r(i); agz_r(i); psi_r(i); r_r(i)];   
    [xn, a_b, uin, Itg] = QrotorSimPX4( xn, xc, a_b, refer, Itg, Ts, uin_r );
    xc = xn;
    
    xn_all(i,:)  = xn';
    a_b_all(i,:) = a_b';
    uin_all(i,:) = uin';
    
end
disp('finish the simulation');

% Parse function outputs
x_m   = xn_all(:,1);
y_m   = xn_all(:,2);
z_m   = xn_all(:,3);
ugb_m = xn_all(:,4);
vgb_m = xn_all(:,5);
wgb_m = xn_all(:,6); 
agx_m = zeros(length(a_b_all), 1); 
agy_m = zeros(length(a_b_all), 1); 
agz_m = zeros(length(a_b_all), 1); 
phi_m = xn_all(:,7);
tht_m = xn_all(:,8);
psi_m = xn_all(:,9);
w_p_m = xn_all(:,10);
w_q_m = xn_all(:,11);
w_r_m = xn_all(:,12);

ail_m = uin_all(:,1);
ele_m = uin_all(:,2);
thr_m = uin_all(:,3);
rud_m = uin_all(:,4);


% % calculate ug, vg, wg (velocity in NED frame) and ag
R = eye(3,3); % rotation matrix transforming body-axis vector to ned-axis vector

ug_m  = zeros(length(ugb_m), 1); 
vg_m  = zeros(length(vgb_m), 1); 
wg_m  = zeros(length(wgb_m), 1); 

for i=1:length(a_b_all)
    phi = phi_m(i);
    tht = tht_m(i);
    psi = psi_m(i);
    
    R = R_Body2NED(phi, tht, psi);    
    
    % convert to NED frame
    ag_m = R * a_b_all(i,:)'; 
    agx_m(i) = ag_m(1);
    agy_m(i) = ag_m(2);
    agz_m(i) = ag_m(3);
    
    vel_g   = R * [ugb_m(i); vgb_m(i); wgb_m(i)];
    ug_m(i) = vel_g(1);
    vg_m(i) = vel_g(2);
    wg_m(i) = vel_g(3);
end


% position, velocity and acceleration
figure(1);

set(0,'defaultlinelinewidth',2)

subplot(3,3,1); plot(t, x_r, 'r--', t, x_m); ylabel('x (m)'); grid on; legend('reference', 'measured'); 
subplot(3,3,4); plot(t, y_r, 'r--', t, y_m); ylabel('y (m)'); grid on;
subplot(3,3,7); plot(t, z_r, 'r--', t, z_m); ylabel('z (m)'); grid on; xlabel('time (s)')

subplot(3,3,2); plot(t, ug_r, 'r--', t, ug_m); ylabel('ug (m/s)'); grid on;
subplot(3,3,5); plot(t, vg_r, 'r--', t, vg_m); ylabel('vg (m/s)'); grid on;
subplot(3,3,8); plot(t, wg_r, 'r--', t, wg_m); ylabel('wg (m/s)'); grid on; xlabel('time (s)')

subplot(3,3,3); plot(t, agx_r, 'r--', t, agx_m); ylabel('ag_x (m/s^2)'); grid on;
subplot(3,3,6); plot(t, agy_r, 'r--', t, agy_m); ylabel('ag_y (m/s^2)'); grid on;
subplot(3,3,9); plot(t, agz_r, 'r--', t, agz_m); ylabel('ag_z (m/s^2)'); grid on; xlabel('time (s)')


% angle and angular rate
figure(2);
subplot(3,2,1); plot(t, phi_m); ylabel('\phi (rad)'); grid on; 
subplot(3,2,3); plot(t, tht_m); ylabel('\theta (rad)'); grid on;
subplot(3,2,5); plot(t, psi_r, 'r--', t, psi_m); legend('reference', 'measured'); 
ylabel('\psi (rad)'); grid on; xlabel('time (s)');

subplot(3,2,2); plot(t, w_p_m); ylabel('p (rad/s)'); grid on;
subplot(3,2,4); plot(t, w_q_m); ylabel('q (rad/s)'); grid on;
subplot(3,2,6); plot(t, r_r, 'r--', t, w_r_m); ylabel('r (rad/s)'); grid on; xlabel('time (s)')


% control inputs
figure(3)
subplot(2,2,1); plot(t, ail_m); ylabel('u1'); grid on; title('control inputs'); legend('measured'); 
subplot(2,2,2); plot(t, ele_m); ylabel('u2'); grid on;
subplot(2,2,3); plot(t, thr_m); ylabel('u3'); grid on;
subplot(2,2,4); plot(t, rud_m); ylabel('u4'); grid on; xlabel('time (s)')


%% comparision of simulation and real fight data
if selRef == 6
    
    %--- manually align the data
    i0 = 395; 
    px4_.t  = px4.t(i0:end);
    px4_.x  = px4.x(i0:end); 
    px4_.y  = px4.y(i0:end); 
    px4_.z  = px4.z(i0:end); 
    px4_.ug = px4.ug(i0:end); 
    px4_.vg = px4.vg(i0:end); 
    px4_.wg = px4.wg(i0:end);
    px4_.agx = px4.agx(i0:end); 
    px4_.agy = px4.agy(i0:end); 
    px4_.agz = px4.agz(i0:end);
    px4_.phi = px4.phi(i0:end); 
    px4_.tht = px4.tht(i0:end); 
    px4_.psi = px4.psi(i0:end);
    px4_.p   = px4.p(i0:end); 
    px4_.q   = px4.q(i0:end); 
    px4_.r   = px4.r(i0:end); 
    
    px4_.t = px4_.t - px4_.t(1); 
    
    figure(4)
    plot(x_r, y_r, 'r--', x_m, y_m, px4_.x, px4_.y); grid on; 
    legend('Reference', 'Simulation', 'Flight'); title('2D Trajectory');
    
    
    figure(5)
    subplot(3,3,1); plot(t, x_r, 'r--', t, x_m, px4_.t, px4_.x); ylabel('x (m)'); grid on; title('Position'); legend('Reference', 'Simulation', 'Flight');  
    subplot(3,3,4); plot(t, y_r, 'r--', t, y_m, px4_.t, px4_.y); ylabel('y (m)'); grid on;
    subplot(3,3,7); plot(t, z_r, 'r--', t, z_m, px4_.t, px4_.z); ylabel('z (m)'); grid on; xlabel('time (s)')

    subplot(3,3,2); plot(t, ug_r, 'r--', t, ug_m, px4_.t, px4_.ug); ylabel('ug (m/s)'); grid on; title('Velocity'); 
    subplot(3,3,5); plot(t, vg_r, 'r--', t, vg_m, px4_.t, px4_.vg); ylabel('vg (m/s)'); grid on;
    subplot(3,3,8); plot(t, wg_r, 'r--', t, wg_m, px4_.t, px4_.wg); ylabel('wg (m/s)'); grid on; xlabel('time (s)')

    subplot(3,3,3); plot(t, agx_r, 'r--', t, agx_m, px4_.t, px4_.agx); ylabel('ag_x (m/s^2)'); grid on; title('Acceleration'); 
    subplot(3,3,6); plot(t, agy_r, 'r--', t, agy_m, px4_.t, px4_.agy); ylabel('ag_y (m/s^2)'); grid on;
    subplot(3,3,9); plot(t, agz_r, 'r--', t, agz_m, px4_.t, px4_.agz); ylabel('ag_z (m/s^2)'); grid on; xlabel('time (s)')

    figure(6)
    subplot(3,2,1); plot(t, phi_m, px4_.t, px4_.phi); ylabel('\phi (rad)'); grid on; title('Euler Angle'); 
    subplot(3,2,3); plot(t, tht_m, px4_.t, px4_.tht); ylabel('\theta (rad)'); grid on;
    subplot(3,2,5); plot(t, psi_r, 'r--', t, psi_m, px4_.t, px4_.psi); legend('Reference', 'Simulation', 'Flight');  
    ylabel('\psi (rad)'); grid on; xlabel('time (s)');

    subplot(3,2,2); plot(t, w_p_m, px4_.t, px4_.p); ylabel('p (rad/s)'); grid on; title('Angular Rate'); 
    subplot(3,2,4); plot(t, w_q_m, px4_.t, px4_.q); ylabel('q (rad/s)'); grid on;
    subplot(3,2,6); plot(t, w_r_m, px4_.t, px4_.r); ylabel('r (rad/s)'); grid on; xlabel('time (s)')
end