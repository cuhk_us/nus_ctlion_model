function [xn, a_b, uin, Itg] = QrotorSimPX4( xn, xc, a_b, ref, Itg, Ts, uin_r )
% please note that this model is developed for CT-Lion (Quadrotor)

% Single Step Simulation of Quadrotor with Inner Loop Converter Only!
%       [xn1, a_b, uin] = QrotorSim( xn, xc, a_b, ref, Ts )
% xn1 = [pos; v_b; att; omg; xun] ----------- updated system state
% uin = [ail; ele; thr; rud] ---------------- current input to the inner loop 
% a_b1= [a_bx; a_by; a_bz] ------------------ updated body-axis acceleration m/s^2
% xn  = [pos; v_b; att; omg; xun; a_b] ------ current system state
%        pos = [x; y; z] -------------------- current ground-axis position, m
%        vel = [u; v; w] -------------------- current body-axis velocity, m/s
%        att = [phi; tht; psi] -------------- current attitude angles, rad
%        omg = [p; q; r] -------------------- current angular rates, rad/s
%        xun = [s_p; ss_p; s_q, ss_q] ------- current unmeasurable variables
% xc  = [pos; v_b; att; omg; xun] ----------- current measured system state for control 
%        pos = [x; y; z] -------------------- current ground-axis position, m
%        vel = [u; v; w] -------------------- current body-axis velocity, m/s
%        att = [phi; tht; psi] -------------- current attitude angles, rad
%        omg = [p; q; r] -------------------- current angular rates, rad/s
%        xun = [s_p; ss_p; s_q, ss_q] ------- current unmeasurable variables
% a_b = [a_bx; a_by; a_bz] ------------------ current body-axis acceleration m/s^2
% ref = [pos_r; vel_r; acc_r, psi_r; r_r] --- current reference input
%        pos_r ------------------------------ desired ground position, m/s
%        vel_r ------------------------------ desired ground velocity, m/s
%        acc_r ------------------------------ desired ground acceleration, m/s^2
%        psi_r ------------------------------ desired heading, rad
%        r_r   ------------------------------ desired heading rate, rad/s
% Itg = [ipos_err; ipsi_err]----------------- the integration error of the
%                                             position and  psi, temperory 
%                                             values
% psi and the reference
% Ts ---------------------------------------- sampling interval of control, sec

global FRAME_TYPE 

FRAME_TYPE =  'OCT'; % 'QUAD' or 'OCT'

global  g  Kt1 Kt2 Kq CM CT  m  Ixx  Iyy  Izz Lm

g   = 9.781;        % gravitational acceleration, m/s^2

% Model parameters
switch FRAME_TYPE 
    case 'QUAD'
        
        Kt1 = 9.0426e-06;   % Thrust coefficient, counter clock wise
        Kt2 = 9.0426e-06;   % Thrust coefficient, clock wise
        Kq  = 1.1808e-07;   % Drag coefficient

        CM  = 1063.4; 
        CT  = -13.82; 

        Kw1 = [CM CT] ;     % the gain of PWM to ration speed, counter clock wise. 
        Kw2 = [CM CT];      % the gain of PWM to ration speed, clock wise

        Lm  = 0.19;         % the length of the arm of the platform, m

        m   = 1.5;          % total mass of helicopter, kg
        
        Ixx = 0.0118;       % rolling moment of inertia, kg*m^2  (real measured value)
        Iyy = 0.0124;       % pitching moment of inertia, kg*m^2 (ream measured value)
        Izz = 0.0224;       % yawing moment of inertia, kg*m^2   (real measured value)

    case 'OCT'

        Kt1 = 9.0426e-06;   % Thrust coefficient, counter clock wise
        Kt2 = 9.0426e-06;   % Thrust coefficient, clock wise
        Kq  = 1.1808e-07;   % Drag coefficient
        Kq1 = Kq;
        Kq2 = Kq; 

        CM  = 1063.4; 
        CT  = -13.82; 

        Kw1 = [CM CT] ;     % the gain of PWM to ration speed, counter clock wise. 
        Kw2 = [CM CT];      % the gain of PWM to ration speed, clock wise

        Lm  = 0.19;         % the length of the arm of the platform, m

        m   = 2.431;          % total mass of helicopter, kg
        
        Ixx = 0.029;       % rolling moment of inertia, kg*m^2  (real measured value)
        Iyy = 0.0195;       % pitching moment of inertia, kg*m^2 (ream measured value)
        Izz = 0.0349;       % yawing moment of inertia, kg*m^2   (real measured value)
end

%% Inputs
pos_r = ref(1:3);
vel_r = ref(4:6);
acc_r = ref(7:9);
psi_r = ref(10);
r_r   = ref(11); 
psi_r = psi_r - floor( (psi_r+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]


%% Single Control Step, Multiple Simulation Steps
ts = 0.005;     % step size for model simulation, sec
Ts = max( ts, Ts );

%% Operating Point
vel0 = [0; 0; 0];
att0 = [0; 0; 0];
omg0 = [0; 0; 0];
xun0 = [0; 0; 0];
uin0 = [0; 0; 0; 0];
wnd0 = [0; 0; 0];




%% roll and pitch control law

% ksi   = 0.8; 
% wn    = 2 * 1.55 * pi *3;  % 1.55 Hz bandwidth for the Blacklion 168
% F_phi = -[wn^2 2*ksi*wn];
% G_phi = -F_phi(1);
%  
% ksi   = 0.8; 
% wn    = 2 * 1.55 * pi * 3;  % 1.55 Hz bandwidth for the Blacklion 168
% F_tht = -[wn^2 2*ksi*wn];
% G_tht = -F_tht(1);

% use the parameters in px4 of CT-Lion

MC_ROLL_P       = 187; 
MC_ROLLRATE_P   = 30;
MC_ROLLRATE_D   = 1.6;

F_phi = -[MC_ROLL_P, MC_ROLLRATE_P, MC_ROLLRATE_D];
%G_phi = -F_phi(1);


MC_PITCH_P      = 187;
MC_PITCHRATE_P  = 30; 
MC_PITCHRATE_D  = 1.6; 

F_tht = -[MC_PITCH_P, MC_PITCHRATE_P, MC_PITCHRATE_D];
%G_tht = -F_tht(1);

%% xy outer-loop controller
wn      = 0.7547; %% CT-Lion
sigma   = 0.5926;
ki      = 0.7024;

eps   = 1;
F_xy  = [(wn^2+2*sigma*wn*ki)/eps^2, (2*sigma*wn+ki)/eps, -ki*wn^2/eps^3, -(wn^2+2*sigma*wn*ki)/eps^2, -(2*sigma*wn+ki)/eps];

%% z outer-loop controller
wn      = 1.5515; %% CT-Lion
sigma   = 0.8983;
ki      = 0.2127;

eps   = 1;
F_z   = [(wn^2+2*sigma*wn*ki)/eps^2, (2*sigma*wn+ki)/eps, -ki*wn^2/eps^3, -(wn^2+2*sigma*wn*ki)/eps^2, -(2*sigma*wn+ki)/eps];


%% psi controller design
% wn    = 0.2;
% sigma = 1.2;
% eps   = 1*10;
% F_c   = [2*wn*sigma/eps^2, -(wn^2)/eps, -2*wn*sigma/eps^2];

% ksi   = 0.8; 
% wn    = 2 * 0.5 * pi * 2;  % 1.55 Hz bandwidth for the Blacklion 168
% F_psi = -[wn^2 2*ksi*wn];
% G_psi = -F_psi(1);

% CT-Lion
MC_YAW_P     = 7.6984; 
MC_YAWRATE_P = 4.04;
MC_YAW_I     = 7.07;  % obsolute value

F_psi = -[MC_YAW_P, MC_YAWRATE_P];
G_psi = -F_psi(1);

%% current states
pos = xc(1:3);
v_b = xc(4:6);
phi = xc(7) - floor( (xc(7)+pi) / (2*pi) ) * (2*pi);  % convert to [-pi pi]
tht = xc(8) - floor( (xc(8)+pi) / (2*pi) ) * (2*pi);
psi = xc(9) - floor( (xc(9)+pi) / (2*pi) ) * (2*pi);
omg = xc(10:12);
%xun = xc(13:15);
domg = xc(13:15); 

R = eye(3,3); % transformation from body coorindates to ground coordinates
R(1,1) = cos(psi)*cos(tht);
R(1,2) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
R(1,3) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
R(2,1) = sin(psi)*cos(tht);
R(2,2) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
R(2,3) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
R(3,1) =-sin(tht);
R(3,2) = cos(tht)*sin(phi);
R(3,3) = cos(tht)*cos(phi);
v_g    = R * v_b; 


%% outer-loop control
ipos_err = Itg(1:3) + Ts * ( pos_r - pos ); 

psi_err  = ( psi_r - psi ); % the error of psi_r and psi
psi_err  = psi_err - floor( (psi_err+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]
ipsi_err = Itg(4) + Ts * psi_err; 
ipsi_err = ipsi_err - floor( (ipsi_err+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]

Itg(1:3) = ipos_err; 
Itg(4)   = ipsi_err; 

%% xy 
agx_r    = [F_xy, 1] * [ pos_r(1); vel_r(1); ( -1 * ipos_err(1) ); pos(1); v_g(1); acc_r(1)]; 
agy_r    = [F_xy, 1] * [ pos_r(2); vel_r(2); ( -1 * ipos_err(2) ); pos(2); v_g(2); acc_r(2)]; 

thr_agx = 5; % threshold
thr_agy = 5; 

if (agx_r < -thr_agx) agx_r = -thr_agx; end
if (agx_r >  thr_agx) agx_r =  thr_agx; end
if (agy_r < -thr_agy) agy_r = -thr_agy; end
if (agy_r >  thr_agy) agy_r =  thr_agy; end


%% z
agz_r    = [F_z, 1] * [ pos_r(3); vel_r(3); ( -1 * ipos_err(3) ); pos(3); v_g(3); acc_r(3)]; 
% a_g_r    = [agx_r; agy_r; agz_r];

thr_agz = 5; % threshold

if (agz_r < -thr_agz) agz_r = -thr_agz; end
if (agz_r >  thr_agz) agz_r =  thr_agz; end


%% psi
%dpsi_r   = [F_c,  1] * [ psi_r; ( ipsi-ipsi_r );  psi;  r_r]; 
%dpsi_r   = [F_c,  1] * [ psi_r; ( -ipsi_err );  psi;  r_r]; 

%dpsi_r = F_c(1) * ( psi_err) + F_c(2) * (-1) * ipsi_err + r_r;

%if (dpsi_r<-0.5) dpsi_r = -0.5; end
%if (dpsi_r> 0.5) dpsi_r =  0.5; end




%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  Command Conversion and inner loop control

selCommandConversion = 2; 

% if ( selCommandConversion == 1) % using Wang Fei's method
%     % dc_thr2w   = -5.7538;
%     % dc_ele2tht =  0.6151;
%     % dc_ail2phi =  0.6151;
%     % dc_rud2r   =  3.3721;
%     % 
%     % damping_u  =  0.09508;
%     % damping_v  =  0.09508;
%     % ratio_u    = -8.661;
%     % ratio_v    =  8.661;
%     % 
%     % ail  = ( v_b(2) * damping_v + a_b_r(2) ) / ratio_v / dc_ail2phi;
%     % ele  = ( v_b(1) * damping_u + a_b_r(1) ) / ratio_u / dc_ele2tht;
%     % thr  = wb_r / dc_thr2w;
%     % rud  = dpsi_r / dc_rud2r; 
% 
%     R_psi  = [ cos(psi) sin(psi) 0; 
%               -sin(psi) cos(psi) 0; 
%                      0        0  1]; 
%     a_g_r2 = R_psi * ( a_g_r - [0; 0; g]); % convert the acceleration to the UAV heading coordinate system
% 
% 
%     abz_r  = a_g_r2(3) / ( cos(tht) * cos(phi) ); % calculate the reference for abz, phi, and theta
%     % tht_r  = asin( a_g_r2(1) / abz_r );
%     % phi_r  = asin( - a_g_r2(2) / ( abz_r * cos(tht_r)) );
%     phi_r = asin( -a_g_r2(2) / abz_r  );
%     tht_r = asin(  a_g_r2(1) / ( abz_r*cos(phi_r) )  );
% 
% 
%     %% For inner loop control
%     u_p = Ixx * ( F_phi * [phi; omg(1)] + G_phi * phi_r );
%     u_q = Iyy * ( F_tht * [tht; omg(2)] + G_tht * tht_r );
%     %u_r = Izz * ( F_psi * [psi; omg(3)] + G_psi * psi_r );
%     u_r = Izz * ( F_psi(1) * (-1) * psi_err + F_psi(2) * omg(3) );
%     %u_r = Izz * dpsi_r;
%     u_z = m * abz_r;
% 
% % u_p = uin_r(1);
% % u_q = uin_r(2);
% % u_r = uin_r(3);
% % u_z = uin_r(4); 
% end

% simulate the command conversion and innner loop control in pixhawk

% agx_r = 0.2; 
% agy_r = 0;
% agz_r = 0;

if ( selCommandConversion == 2) 
    
    SIGMA  = 0.000001; 
    
    thrust_sp       =   [agx_r; agy_r; agz_r - g];
    
    att_sp.yaw_body =   psi_r; 
    
    % calcuate attitude setpoint from thrust vector
    % thrust_sp_fixed     =   [ thrust_sp(1); thrust_sp(2); -0.5];
    thrust_sp_fixed     =   [ thrust_sp(1); thrust_sp(2); thrust_sp(3) ];
    thrust_abs_fixed    =   norm(thrust_sp_fixed); 
    
    % if ( -thrust_sp(3) > SIGMA)
    if ( thrust_abs_fixed > SIGMA)
        body_z = -thrust_sp_fixed / thrust_abs_fixed;
    else
        % no thrust, set Z axis to safe value 
        body_z      =   zeros( 3, 1 );
        body_z(3)   =   1.0;
    end
    
    % vector of desired yaw direction in XY plane, rotated by PI/2
    y_C = [ -sin(psi_r); cos(psi_r); 0 ];
    
    if ( abs( body_z(3) ) > SIGMA )
        
        % desired body_x axis, orthogonal to body_z
        body_x      =   cross(y_C, body_z);
        
        % keep nose to front while inverted upside down
        if ( body_z(3) < 0.0 )
           body_x   =   -body_x;
        end
        
        body_x      =   body_x / norm(body_x);
    else
        % desired thrust is in XY plane, set X downside to construct correct matrix,
		% but yaw component will not be used actually
        body_x      =   zeros(3,1);
        body_x(3)   =   1.0;     
    end
    
    % desired body_y axis
    body_y = cross( body_z, body_x );
    
    
    % fill rotation matrix
    R = [ body_x, body_y, body_z ];
    
    % copy rotation matrix to attitude setpoint topic
    att_sp.R            =   R;
    
    % calculate euler angles, for logging only, must not be used for control
    euler               =   matrix_to_euler(R); 
    att_sp.roll_body    =   euler(1);
    att_sp.pitch_body   =   euler(2);
    % yaw already used to construct rotation matrix, but actual rotation matrix can have different yaw near singularity
    
    %% ********************************************************************
    %  attitude control
    v_att_sp    =   att_sp;
    
    % rotation matrix in _att_sp is not valid, use euler angles instead 
    R_sp        =   matrix_from_euler( v_att_sp.roll_body, v_att_sp.pitch_body, v_att_sp.yaw_body ); 
    
    % rotation matrix for current state
    R           =   matrix_from_euler( phi, tht, psi );
    
    
    % all input data is ready, run controller itself 
    
    % try to move thrust vector shortest way, because yaw response is slower than roll/pitch
    R_z         =   R( :, 3 );
    R_sp_z      =   R_sp( :, 3 );
    
    % axis and sin(angle) of desired rotation 
    e_R         =   R' * ( cross( R_z, R_sp_z) );
    
    % calculate angle error 
    e_R_z_sin   =   norm( e_R );
    e_R_z_cos   =   dot( R_z, R_sp_z );
    
    % calculate weight for yaw control
    yaw_w       =   R_sp(3, 3) * R_sp(3, 3);
    
    % calculate rotation matrix after roll/pitch only rotation
    if ( e_R_z_sin > 0.0 ) 
		
        % get axis-angle representation 
		e_R_z_angle     =   atan2( e_R_z_sin, e_R_z_cos );
		e_R_z_axis      =   e_R / e_R_z_sin;

		e_R             =   e_R_z_axis * e_R_z_angle;

		% cross product matrix for e_R_axis 
		e_R_cp          =   zeros(3,3);
		e_R_cp(1, 2)    =  -e_R_z_axis(3);
		e_R_cp(1, 3)    =   e_R_z_axis(2);
		e_R_cp(2, 1)    =   e_R_z_axis(3);
		e_R_cp(2, 3)    =  -e_R_z_axis(1);
		e_R_cp(3, 1)    =  -e_R_z_axis(2);
		e_R_cp(3, 2)    =   e_R_z_axis(1);

		% rotation matrix for roll/pitch only rotation 
		R_rp = R * ( eye(3) + e_R_cp * e_R_z_sin + e_R_cp * e_R_cp * (1.0 - e_R_z_cos) );

	else 
		% zero roll/pitch rotation 
		R_rp = R;
    end
    
    % R_rp and R_sp has the same Z axis, calculate yaw error 
	R_sp_x  =   R_sp(:, 1);  
	R_rp_x  =   R_rp(:, 1); 
	
    e_R(3) = atan2( dot( cross( R_rp_x, R_sp_x ), R_sp_z ), dot(R_rp_x, R_sp_x) ) * yaw_w;

% 	if ( e_R_z_cos < 0.0 )
% 		% for large thrust vector rotations use another rotation method:
% 		% calculate angle and axis for R -> R_sp rotation directly
% 		math::Quaternion q;
% 		q.from_dcm(R.transposed() * R_sp);
% 		math::Vector<3> e_R_d = q.imag();
% 		e_R_d.normalize();
% 		e_R_d *= 2.0f * atan2f(e_R_d.length(), q(0));
% 
% 		% use fusion of Z axis based rotation and direct rotation */
% 		float direct_w = e_R_z_cos * e_R_z_cos * yaw_w;
% 		e_R = e_R * (1.0f - direct_w) + e_R_d * direct_w;
%     end

    if  e_R_z_cos < 0.0 
        
		% for large thrust vector rotations use another rotation method:
		% calculate angle and axis for R -> R_sp rotation directly
        q = dcm2quat( R' * R_sp ); 
        
		e_R_d = q(2:4)'; 
        
		e_R_d = normc(e_R_d);
        
		e_R_d =  e_R_d * 2.0 * atan2( norm(e_R_d), q(1));

		% use fusion of Z axis based rotation and direct rotation
		direct_w = e_R_z_cos * e_R_z_cos * yaw_w;
		e_R = e_R * (1.0 - direct_w) + e_R_d * direct_w;
        
    end

	angle_error(1)  =   e_R(1);
	angle_error(2)  =   e_R(2);
	angle_error(3)  =   e_R(3) - floor( ( e_R(3) + pi ) / ( 2 * pi ) ) * ( 2 * pi ); % convert to [-pi pi]
    
    
    %%%%%%%%%%  For inner loop control  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    u_p = Ixx * ( F_phi(1) * (-1) * angle_error(1) + F_phi(2) * omg(1) + F_phi(3) * domg(1) );
    u_q = Iyy * ( F_tht(1) * (-1) * angle_error(2) + F_tht(2) * omg(2) + F_tht(3) * domg(2) ); 
    u_r = Izz * ( F_psi(1) * (-1) * angle_error(3) + F_psi(2) * omg(3) );
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % u_z             =   m * thrust_sp(3);
    u_z             =   m * (-1) * norm( thrust_sp );
    
end


%% ESC and motor control

switch FRAME_TYPE 
    case 'QUAD'
        omega2_u = [ -Lm*Kt1/sqrt(2)  Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
                      Lm*Kt1/sqrt(2) -Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
                      Kq                         Kq             -Kq               -Kq;
                     -Kt1                       -Kt1            -Kt2              -Kt2];      
        u_omega2 = omega2_u^-1;

        omega2 = u_omega2 * [u_p; u_q; u_r; u_z];   

        if omega2(1) < 0; omega2(1)=0; end
        if omega2(2) < 0; omega2(2)=0; end
        if omega2(3) < 0; omega2(3)=0; end
        if omega2(4) < 0; omega2(4)=0; end

        omega  = sqrt(omega2);

        %% calculate the PWM signal for the motors based on first order approximation
        uin(1) = ( omega(1) - Kw1(2) ) / Kw1(1); 
        uin(2) = ( omega(2) - Kw1(2) ) / Kw1(1);
        uin(3) = ( omega(3) - Kw2(2) ) / Kw2(1);
        uin(4) = ( omega(4) - Kw2(2) ) / Kw2(1);

        %% check the range
        for ii = 1:4
            if uin(ii) <0
                uin(ii) =0;
            end

            if uin(ii) >1
                uin(ii) = 1;
            end
        end 

    case 'OCT'
        mot_num = 8; 
        
        Den = 4*(Kq1*Kt2 + Kt1*Kq2);
        
        u_omega2(1,1) = -1.4142*Kq2/(Lm*Den);
        u_omega2(1,2) =  1.4142*Kq2/(Lm*Den);
        u_omega2(1,3) =  Kt2/Den;
        u_omega2(1,4) =  Kq2/Den;

        u_omega2(2,1) =  1.4142*Kq2/(Lm*Den);
        u_omega2(2,2) = -1.4142*Kq2/(Lm*Den);
        u_omega2(2,3) =  Kt2/Den;
        u_omega2(2,4) =  Kq2/Den;

        u_omega2(3,1) =  1.4142*Kq1/(Lm*Den);
        u_omega2(3,2) =  1.4142*Kq1/(Lm*Den);
        u_omega2(3,3) = -Kt1/Den;
        u_omega2(3,4) =  Kq1/Den;

        u_omega2(4,1) = -1.4142*Kq1/(Lm*Den);
        u_omega2(4,2) = -1.4142*Kq1/(Lm*Den);
        u_omega2(4,3) = -Kt1/Den;
        u_omega2(4,4) =  Kq1/Den;

        u_omega2(5,1) = -1.4142*Kq1/(Lm*Den);
        u_omega2(5,2) =  1.4142*Kq1/(Lm*Den);
        u_omega2(5,3) = -Kt1/Den;
        u_omega2(5,4) =  Kq1/Den;

        u_omega2(6,1) =  1.4142*Kq1/(Lm*Den);
        u_omega2(6,2) = -1.4142*Kq1/(Lm*Den);
        u_omega2(6,3) = -Kt1/Den;
        u_omega2(6,4) =  Kq1/Den;

        u_omega2(7,1) =  1.4142*Kq2/(Lm*Den);
        u_omega2(7,2) =  1.4142*Kq2/(Lm*Den);
        u_omega2(7,3) =  Kt2/Den;
        u_omega2(7,4) =  Kq2/Den;

        u_omega2(8,1) = -1.4142*Kq2/(Lm*Den);
        u_omega2(8,2) = -1.4142*Kq2/(Lm*Den);
        u_omega2(8,3) =  Kt2/Den;
        u_omega2(8,4) =  Kq2/Den;
        
        omega2 = u_omega2 * [u_p; u_q; u_r; -u_z]; % thrust is positive in px4 code  

        for ii = 1:mot_num
            if omega2(ii) < 0; omega2(ii)=0; end
        end

        omega  = sqrt(omega2);

        %% calculate the PWM signal for the motors based on first order approximation
        uin(1) = ( omega(1) - Kw1(2) ) / Kw1(1); 
        uin(2) = ( omega(2) - Kw1(2) ) / Kw1(1);
        uin(3) = ( omega(3) - Kw2(2) ) / Kw2(1);
        uin(4) = ( omega(4) - Kw2(2) ) / Kw2(1);
        
        uin(5) = ( omega(5) - Kw2(2) ) / Kw2(1);
        uin(6) = ( omega(6) - Kw2(2) ) / Kw2(1);
        uin(7) = ( omega(7) - Kw1(2) ) / Kw1(1); 
        uin(8) = ( omega(8) - Kw1(2) ) / Kw1(1);
       
        %% check the range
        for ii = 1:mot_num
            if uin(ii) <0
                uin(ii) =0;
            end

            if uin(ii) >1
                uin(ii) = 1;
            end
        end 
        
end

%uin = [ail; ele; thr; rud];

% % current control
% a_g_c = Go * [ pos_r; vel_r; acc_r ] + Fo * [ pos; v_g ];
% a_b_c = R' * a_g_c;
% dlt_c = CC(1,:) * a_b_c;
% phi_c = CC(2,:) * a_b_c;
% tht_c = CC(3,:) * a_b_c;
% psi_c = psi_r - psi;
% psi_c = psi_c - floor( (psi_c+pi) / (2*pi) ) * (2*pi);
% yaw = -psi_c;
% dui = Gi * [phi_c; tht_c; psi_c] ...
%     + Fi * ( [phi; tht; yaw; omg; xun] - [att0; omg0; xun0] );
% uin = [dlt_c; dui] + uin0;
% uin = max( min( uin, 1 ), -1 );



%% state update
for k = 1:Ts/ts
    if k == 1
        [k1, a_b] = QrotorDynMod( xn(1:12), uin, [0;0;0] );
        %k1 = QrotorDynMod( xn, uin , [0;0;0] );
    else
        k1 = QrotorDynMod( xn(1:12), uin , [0;0;0] );
    end
    k2 = QrotorDynMod( xn(1:12)+ts/2*k1, uin , [0;0;0] );
    k3 = QrotorDynMod( xn(1:12)+ts/2*k2, uin , [0;0;0] );
    k4 = QrotorDynMod( xn(1:12)+ts  *k3, uin , [0;0;0] );
    xn(1:12) = xn(1:12) + ts/6 * ( k1 + 2*k2 + 2*k3 + k4 );
    
    % calculate omg_r, since psi is modelled using a first order system 
    % xn(12) =  ( k1(9) + 2*k2(9) + 2*k3(9) + k4(9) ) / 6;
    
    % compute the angular acceleration
    xn(13:15) = ( k1(10:12) + 2*k2(10:12) + 2*k3(10:12) + k4(10:12) ) / 6;
end

%% calculate a_b
xn(7) = xn(7) - floor( (xn(7)+pi) / (2*pi) ) * (2*pi);
xn(8) = xn(8) - floor( (xn(8)+pi) / (2*pi) ) * (2*pi);
xn(9) = xn(9) - floor( (xn(9)+pi) / (2*pi) ) * (2*pi);
phi = xn(7);
tht = xn(8);
psi = xn(9); 
psi = psi - floor( (psi+pi) / (2*pi) ) * (2*pi); % convert to [-pi pi]
xn(9) = psi; 

% phi = xn(7) - floor( (xn(7)+pi) / (2*pi) ) * (2*pi);
% tht = xn(8) - floor( (xn(8)+pi) / (2*pi) ) * (2*pi);
% psi = xn(9) - floor( (xn(9)+pi) / (2*pi) ) * (2*pi);

R(1,1) = cos(psi)*cos(tht);
R(1,2) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
R(1,3) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
R(2,1) = sin(psi)*cos(tht);
R(2,2) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
R(2,3) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
R(3,1) =-sin(tht);
R(3,2) = cos(tht)*sin(phi);
R(3,3) = cos(tht)*cos(phi);

a_b = R' * ( R * xn(4:6) - v_g ) / Ts; % differentiate ground velocity





function varargout = QrotorDynMod( x, u, wnd )
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%        Linear Dynamic Model of Quadrotor UAV                           %%%%
%%%%        Quadrotor with Naja attitude stability augmentation             %%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% [dx,a_b] = QrotorDynMod( x, u, wnd )
% x   = [pos; vel; att; omg; xun] ------- current system state
% pos = [x; y; z] ----------------------- current ground-axis position, m
% vel = [u; v; w] ----------------------- current body-axis velocity, m/s%
% att = [phi; theta; psi] --------------- current attitude angles, rad
% omg = [p; q, r] ----------------------- current derivatives of attitude angles, rad/s
% xun = [d2phi; d3phi; d2tht; d3tht] ---- current 2nd and 3rd order derivatives of attitude angles
% u   = [lat; lon; thr; ped ] in [-1 1] without units
% wnd = [ wnd_u; wnd_v; wnd_w ] body-axis wind velocity as disturbance, m/s
% dx  delat of x
%
% Coded by Lin Feng    Modeled by Peng Tao   Directed by Ben M. Chen
% Copyright 2013 research UAV team, National University of Singapore
% Updated: 28 Aug 2013

ts = 0.005;     % step size for model simulation, sec

global FRAME_TYPE 

global  g  Kt1 Kt2 Kq CM CT  m  Ixx  Iyy  Izz Lm

%% Model paramters

rho     = 1.292;    % air density, kg/m^3
Jr      = 0;        % rotor inertia
OMG_r   = 0;        % rotation speed of the motor


%% Parse Function Inputs
% u1 = u(1);          % input to the esc
% u2 = u(2);
% u3 = u(3);
% u4 = u(4);

vel_u = x(4);       % m/s
vel_v = x(5);       % m/s
vel_w = x(6);       % m/s
phi   = x(7);       % rad
tht   = x(8);       % rad
psi   = x(9);       % rad
omg_p = x(10);      % rad/s
omg_q = x(11);      % rad/s
omg_r = x(12);      % rad/s
% b1s   = x(13);      % rad
% b1c   = x(14);      % rad
% r_c   = x(15);      % none

% u_a = vel_u - wnd(1);
% v_a = vel_v - wnd(2);
% w_a = vel_w - wnd(3);

R = eye(3,3); % rotation matrix transforming body-axis vector to ned-axis vector
R(1,1) = cos(psi)*cos(tht);
R(1,2) = cos(psi)*sin(tht)*sin(phi) - sin(psi)*cos(phi);
R(1,3) = cos(psi)*sin(tht)*cos(phi) + sin(psi)*sin(phi);
R(2,1) = sin(psi)*cos(tht);
R(2,2) = sin(psi)*sin(tht)*sin(phi) + cos(psi)*cos(phi);
R(2,3) = sin(psi)*sin(tht)*cos(phi) - cos(psi)*sin(phi);
R(3,1) = -sin(tht);
R(3,2) = cos(tht)*sin(phi);
R(3,3) = cos(tht)*cos(phi);


%% the thrust of the rotors
%  
%  configuration of the motor of the UAV 
% 
%  Quad
%   3(cw)   1(ccw)
%       \  / 
%        \/
%        /\   
%       /  \
%  2(ccw)   4(cw)
%
%  Oct
% 3(cw),7(ccw)   1(ccw), 5(cw)
%           \  / 
%            \/
%            /\   
%           /  \
% 2(ccw),6(cw)  4(cw),8(ccw)

% pwm2omg_ccw = [ 224.7527, -461.6444,  529.3261,    4.0788];
% omg2Tz_ccw  = [ 0.0005,   -0.0137,    0.1698];
% omg2Rz_ccw  = [ 0.000014942636333,   -0.000607079456087,  0.009490512613259];
% 
% pwm2omg_cw  = [ 249.2520, -496.6389,  537.6168,    3.4888];
% omg2Tz_cw   = [ 0.0005,   -0.0155,    0.2126];
% omg2Rz_cw   = [ 0.000016000332401,  -0.000731438959227,   0.011212137724635];

%%%%%%%%%%%%%%%%%%%%%%%%% CT-Lion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% CT-Lion
Kt1 = 9.0426e-06;   % Thrust coefficient, counter clock wise
Kt2 = 9.0426e-06;   % Thrust coefficient, clock wise
Kq  = 1.1808e-07;   % Drag coefficient

CM  = 1063.4; 
CT  = -13.82; 

%Kw1 = [CM CT] ;     % the gain of PWM to ration speed, counter clock wise. 
%Kw2 = [CM CT];      % the gain of PWM to ration speed, clock wise

pwm2omg_ccw = [ 0, 0,  CM,   CT ];
omg2Tz_ccw  = [ Kt1,   0,    0];
omg2Rz_ccw  = [ Kq,    0,    0];

pwm2omg_cw  = [ 0, 0,  CM,   CT];
omg2Tz_cw   = [ Kt2,   0,    0];
omg2Rz_cw   = [ Kq,    0,    0];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
switch FRAME_TYPE 
    case 'QUAD'

        mot_num = 4; 
        
        Omg_(1) = pwm2omg_ccw * [ u(1)^3; u(1)^2; u(1); 1]; 
        Omg_(2) = pwm2omg_ccw * [ u(2)^3; u(2)^2; u(2); 1]; 
        Omg_(3) = pwm2omg_cw  * [ u(3)^3; u(3)^2; u(3); 1]; 
        Omg_(4) = pwm2omg_cw  * [ u(4)^3; u(4)^2; u(4); 1]; 

        %Omg_ = [Omg1_, Omg2_, Omg3_, Omg4_]; 

    case 'OCT'

        mot_num = 8; 
        
        Omg_(1) = pwm2omg_ccw * [ u(1)^3; u(1)^2; u(1); 1]; 
        Omg_(2) = pwm2omg_ccw * [ u(2)^3; u(2)^2; u(2); 1]; 
        
        Omg_(3) = pwm2omg_cw  * [ u(3)^3; u(3)^2; u(3); 1]; 
        Omg_(4) = pwm2omg_cw  * [ u(4)^3; u(4)^2; u(4); 1]; 
        
        Omg_(5) = pwm2omg_cw  * [ u(5)^3; u(5)^2; u(5); 1]; 
        Omg_(6) = pwm2omg_cw  * [ u(6)^3; u(6)^2; u(6); 1]; 
        
        Omg_(7) = pwm2omg_ccw * [ u(7)^3; u(7)^2; u(7); 1]; 
        Omg_(8) = pwm2omg_ccw * [ u(8)^3; u(8)^2; u(8); 1]; 
        
end


%% motor dynamics
tao = 0.07;         % time constant of the motor system

[mot_A, mot_B, mot_C, mot_D] = tf2ss( [1/tao], [1 1/tao] );  
mot_ss = ss(mot_A, mot_B, mot_C, mot_D); 

persistent mot_x0 % initial states of each motor

if isempty(mot_x0)
        mot_x0 = zeros(mot_num, 1);
end

mot_t = 0:ts/5:ts;
mot_td = length(mot_t); 

%display(Omg_); 

for m_i = 1:mot_num
    
    mot_u = Omg_(m_i) * ones( mot_td, 1 );
    
    [mot_y, ~, mot_x] = lsim( mot_ss, mot_u, mot_t, mot_x0(m_i) ); 
    
    mot_x0(m_i) = mot_x(end); % update the initial state for next step sim
    
    Omg(m_i) = mot_y(end); % output of the motor system
end

%display(Omg);
%disp('---------------');

% Omg1 = Omg(1);
% Omg2 = Omg(2);
% Omg3 = Omg(3);
% Omg4 = Omg(4);


switch FRAME_TYPE 
    case 'QUAD'
        
        T_rt(1) = omg2Tz_ccw * [ Omg(1)^2; Omg(1); 1]; 
        T_rt(2) = omg2Tz_ccw * [ Omg(2)^2; Omg(2); 1]; 
        T_rt(3) = omg2Tz_cw  * [ Omg(3)^2; Omg(3); 1]; 
        T_rt(4) = omg2Tz_cw  * [ Omg(4)^2; Omg(4); 1]; 

        Q_rt(1) = omg2Rz_ccw * [ Omg(1)^2; Omg(1); 1];  
        Q_rt(2) = omg2Rz_ccw * [ Omg(2)^2; Omg(2); 1];  
        Q_rt(3) = omg2Rz_cw  * [ Omg(3)^2; Omg(3); 1];  
        Q_rt(4) = omg2Rz_cw  * [ Omg(4)^2; Omg(4); 1];  
        
        %% rotor force and moments
        Xrt =  0;
        Yrt =  0; 
        Zrt = -( T_rt(1) + T_rt(2) + T_rt(3) + T_rt(4) );

        Lrt = Lm / sqrt(2) * ( -T_rt(1) + T_rt(2) + T_rt(3) - T_rt(4) );
        Mrt = Lm / sqrt(2) * (  T_rt(1) - T_rt(2) + T_rt(3) - T_rt(4) );
        Nrt = Q_rt(1) + Q_rt(2) - Q_rt(3) - Q_rt(4);
              
    case 'OCT'
        
        T_rt(1) = omg2Tz_ccw * [ Omg(1)^2; Omg(1); 1]; 
        T_rt(2) = omg2Tz_ccw * [ Omg(2)^2; Omg(2); 1]; 
        T_rt(3) = omg2Tz_cw  * [ Omg(3)^2; Omg(3); 1]; 
        T_rt(4) = omg2Tz_cw  * [ Omg(4)^2; Omg(4); 1]; 
        T_rt(5) = omg2Tz_cw  * [ Omg(5)^2; Omg(5); 1]; 
        T_rt(6) = omg2Tz_cw  * [ Omg(6)^2; Omg(6); 1]; 
        T_rt(7) = omg2Tz_ccw * [ Omg(7)^2; Omg(7); 1]; 
        T_rt(8) = omg2Tz_ccw * [ Omg(8)^2; Omg(8); 1]; 

        Q_rt(1) = omg2Rz_ccw * [ Omg(1)^2; Omg(1); 1];  
        Q_rt(2) = omg2Rz_ccw * [ Omg(2)^2; Omg(2); 1];  
        Q_rt(3) = omg2Rz_cw  * [ Omg(3)^2; Omg(3); 1];  
        Q_rt(4) = omg2Rz_cw  * [ Omg(4)^2; Omg(4); 1];  
        Q_rt(5) = omg2Rz_cw  * [ Omg(5)^2; Omg(5); 1];  
        Q_rt(6) = omg2Rz_cw  * [ Omg(6)^2; Omg(6); 1];  
        Q_rt(7) = omg2Rz_ccw * [ Omg(7)^2; Omg(7); 1];  
        Q_rt(8) = omg2Rz_ccw * [ Omg(8)^2; Omg(8); 1];
        
        %% rotor force and moments
        Xrt =  0;
        Yrt =  0; 
        Zrt = - sum(T_rt);

        Lrt = Lm / sqrt(2) * ( -T_rt(1) + T_rt(2) + T_rt(3) - T_rt(4) - T_rt(5) + T_rt(6) + T_rt(7) - T_rt(8) );
        Mrt = Lm / sqrt(2) * (  T_rt(1) - T_rt(2) + T_rt(3) - T_rt(4) + T_rt(5) - T_rt(6) + T_rt(7) - T_rt(8) );
        Nrt = Q_rt(1) + Q_rt(2) - Q_rt(3) - Q_rt(4) - Q_rt(5) - Q_rt(6) + Q_rt(7) + Q_rt(8);

end 

% Kt1 = 0.0005;       % Thrust coefficient, counter clock wise
% Kt2 = 0.0005;       % Thrust coefficient, clock wise
% Kq  = 0.000016;     % Drag coefficient
% 
% omega2_u = [ -Lm*Kt1/sqrt(2)  Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
%               Lm*Kt1/sqrt(2) -Lm*Kt1/sqrt(2)  Lm*Kt2/sqrt(2)  -Lm*Kt2/sqrt(2);
%               Kq                         Kq             -Kq               -Kq;
%              -Kt1                       -Kt1            -Kt2              -Kt2];
%          
% U = omega2_u * [ Omg1^2; Omg2^2; Omg3^2; Omg4^2 ]; 
% Lrt = U(1);
% Mrt = U(2);
% Nrt = U(3);
% Zrt = U(4); 


%% compute the drad due to the flight speed

% the motor to motor distance
MTM     = 0.382; 

% radius of the propeller  
r_prop  = 0.12; % 9 x 4.5 inch

A       = 0.5 * MTM^2 + 3 * pi * r_prop^2;     % total area of the platform, m^2

D_u = 2*0.5 * rho * A * abs( sin(tht) ) * vel_u^2; 
D_v = 2*0.5 * rho * A * abs( sin(phi) ) * vel_v^2; 
D_w = 2*0.5 * rho * A * vel_w^2; 


%% resulted forces and moments along body axes
F_u = Xrt - D_u;
F_v = Yrt - D_v;
F_w = Zrt - D_w;
M_p = Lrt;
M_q = Mrt;
M_r = Nrt;


%% Rigid body dynamics
domg_p = ( ( Iyy - Izz ) * omg_q * omg_r + Jr * omg_q * OMG_r + M_p ) / Ixx; % body gyro effect + propeller gyro effect + roll actuator action
domg_q = ( ( Ixx - Izz ) * omg_p * omg_r + Jr * omg_p * OMG_r + M_q ) / Iyy;
domg_r = ( ( Ixx - Iyy ) * omg_p * omg_q + M_r) / Izz;

dphi = [1  sin(phi)*tan(tht)  cos(phi)*tan(tht)] * [omg_p; omg_q; omg_r];
dtht = [0       cos(phi)          -sin(phi)    ] * [omg_p; omg_q; omg_r];
dpsi = [0  sin(phi)*sec(tht)  cos(phi)*sec(tht)] * [omg_p; omg_q; omg_r];

% agx = R(1,:) * [ F_u; F_v; F_w; ] / m ;
% agy = R(2,:) * [ F_u; F_v; F_w; ] / m;
% agz = R(3,:) * [ F_u; F_v; F_w; ] / m + g;

a_u = F_u / m - g * sin(tht);
a_v = F_v / m + g * sin(phi)*cos(tht);
a_w = F_w / m + g * cos(phi)*cos(tht);

dvel_u = a_u - omg_q*vel_w + omg_r*vel_v;
dvel_v = a_v - omg_r*vel_u + omg_p*vel_w;
dvel_w = a_w - omg_p*vel_v + omg_q*vel_u;

v_g  = R * [vel_u; vel_v; vel_w];
dpos = v_g;

  
%% Funtion Outputs
dx = [dpos;             dvel_u; dvel_v; dvel_w;
      dphi; dtht; dpsi; domg_p; domg_q; domg_r;];
if nargout == 1
    varargout{1} = dx;
elseif nargout == 2
    varargout{1} = dx;
    varargout{2} = [F_u; F_v; F_w]/m;
end

