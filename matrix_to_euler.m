function euler = matrix_to_euler(R)  % euler = [roll; pitch; yaw]

euler = zeros(3,1);

euler(2) = asin( -R(3,1) );

if ( abs( euler(2) - pi/2.0 ) < 1.0e-3 ) 
    euler(1) = 0.0;
    euler(3) = atan2( R(2,3) - R(1,2), R(1,3) + R(2,2) ) + euler(1);

elseif ( abs( euler(2) + pi/2.0 ) < 1.0e-3 ) 
	euler(1) = 0.0;
	euler(3) = atan2( R(2,3) - R(1,2), R(1,3) + R(2,2) ) - euler(1);
	 
else 
	euler(1) = atan2( R(3,2), R(3,3) );
	euler(3) = atan2( R(2,1), R(1,1) );

end

